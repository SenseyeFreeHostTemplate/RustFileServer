# RustStaticFileServer

Rust static file server example for return `*.html` or `*.js`, `*.css`, `*.*` files

##### Local run
```bash
cargo run
```

##### Resources:
- [Static file-serving handler for the Iron web framework](https://github.com/iron/staticfile)

##### Free rust hosting
- [Bluemix (IBM)](https://gitlab.com/SenseyeFreeHostTemplate/BluemixRustFileServer)
